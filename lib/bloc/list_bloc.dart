import 'package:bloc/bloc.dart';
import 'package:simple_note/model/note_model.dart';
import 'package:simple_note/view_model/note_view_model.dart';

class PostEvent {}

abstract class PostState {}

class PostUninitialized extends PostState {}

class PostLoaded extends PostState {
  List<NoteModel> posts;

  PostLoaded({this.posts});

  PostLoaded copyWith({List<NoteModel> posts}) {
    return PostLoaded(posts: posts ?? this.posts);
  }
}

class PostBloc extends Bloc<PostEvent, PostState> {
  PostBloc() : super(initialState);

  static PostState get initialState => PostUninitialized();

  @override
  Stream<PostState> mapEventToState(PostEvent event) async* {
    List<NoteModel> posts;
    if (state is PostUninitialized) {
      posts = await NoteViewModel().readData();
      yield PostLoaded(posts: posts);
    } else {
      posts = await NoteViewModel().readData();
      yield PostLoaded(posts: posts);
    }
  }
}
