import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:simple_note/model/note_model.dart';

class ListItem extends StatelessWidget {
  final NoteModel model;

  ListItem({this.model});

  @override
  Widget build(BuildContext context) {
    return Card(
      shadowColor: Colors.orangeAccent,
      child: Row(
        children: [
          Icon(CupertinoIcons.pen),
          SizedBox(
            width: 10.0,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                model.title,
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0),
              ),
              SizedBox(
                height: 10.0,
              ),
              Text(
                model.body,
                style: TextStyle(fontSize: 16.0),
              )
            ],
          )
        ],
      ),
    );
  }
}
