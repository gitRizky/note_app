import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:simple_note/bloc/list_bloc.dart';
import 'package:simple_note/ui/list_item.dart';

import 'create_note_page.dart';
import 'detail_page.dart';

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  // List<NoteModel> _model;
  PostBloc _bloc;

  // @override
  // void initState() {
  //   super.initState();
  //   readData();
  // }

  @override
  Widget build(BuildContext context) {
    _bloc = BlocProvider.of<PostBloc>(context);
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.orangeAccent,
          title: Text("Simple Note"),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.push(context,
                    MaterialPageRoute(builder: (context) => CreateNotePage()))
                .then((value) => _bloc.add(PostEvent()));
          },
          backgroundColor: Colors.orangeAccent,
          child: Icon(CupertinoIcons.add),
        ),
        body: Container(
          margin: EdgeInsets.all(20.0),
          child: BlocBuilder<PostBloc, PostState>(
            builder: (context, state) {
              if (state is PostUninitialized) {
                return Center(
                  child: CircularProgressIndicator(),
                );
              } else {
                PostLoaded postLoaded = state as PostLoaded;
                return ListView.builder(
                  itemCount: postLoaded.posts.length,
                  itemBuilder: (context, index) =>
                      (index < postLoaded.posts.length)
                          ? GestureDetector(
                              onTap: () => Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => DetailPage(
                                                data: postLoaded.posts[index],
                                              )))
                                  .then((value) => _bloc.add(PostEvent())),
                              child: ListItem(
                                model: postLoaded.posts[index],
                              ),
                            )
                          : Container(
                              child: Center(
                                child: CircularProgressIndicator(),
                              ),
                            ),
                );
              }
            },
          ),
        ),
        // Padding(
        //   padding: EdgeInsets.all(10.0),
        //   child: Column(
        //     children: [
        //       Expanded(
        //         child: ListView.builder(
        //             itemCount: _model.length,
        //             itemBuilder: (BuildContext context, int index) {
        //               return GestureDetector(
        //                 onTap: () {
        //                   Navigator.push(
        //                       context,
        //                       MaterialPageRoute(
        //                           builder: (context) => DetailPage(
        //                                 data: _model[index],
        //                               ))).then((value) => readData());
        //                 },
        //                 child: Card(
        //                   child: Padding(
        //                     padding: const EdgeInsets.all(8.0),
        //                     child: Column(
        //                       crossAxisAlignment: CrossAxisAlignment.start,
        //                       children: [
        //                         Text(
        //                           _model[index].title,
        //                           style: TextStyle(
        //                               fontWeight: FontWeight.bold,
        //                               fontSize: 16.0),
        //                         ),
        //                         SizedBox(
        //                           height: 10.0,
        //                         ),
        //                         Text(_model[index].body)
        //                       ],
        //                     ),
        //                   ),
        //                 ),
        //               );
        //             }),
        //       ),
        //     ],
        //   ),
        // ),
      ),
    );
  }

  // void readData() async {
  //   await NoteViewModel().readData().then((value) {
  //     setState(() {
  //       _model = value;
  //     });
  //   });
  // }
}
