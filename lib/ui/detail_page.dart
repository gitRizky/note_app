import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:simple_note/model/note_model.dart';
import 'package:simple_note/view_model/note_view_model.dart';

class DetailPage extends StatefulWidget {
  final NoteModel data;

  DetailPage({this.data});
  @override
  _DetailPageState createState() => _DetailPageState();
}

class _DetailPageState extends State<DetailPage> {
  TextEditingController title = new TextEditingController();
  TextEditingController body = new TextEditingController();

  @override
  void initState() {
    super.initState();
    title.text = widget.data.title;
    body.text = widget.data.body;
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.orangeAccent,
          title: Text("Create Note"),
        ),
        body: Container(
          margin: EdgeInsets.all(10.0),
          child: Column(
            children: [
              TextField(
                controller: title,
                decoration: InputDecoration(hintText: "title"),
              ),
              TextField(
                controller: body,
                decoration: InputDecoration(hintText: "body"),
              )
            ],
          ),
        ),
        bottomNavigationBar: Padding(
          padding: EdgeInsets.only(bottom: 30.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                width: 120.0,
                height: 40.0,
                child: ElevatedButton(
                    style: ButtonStyle(
                        backgroundColor:
                            MaterialStateProperty.all(Colors.orangeAccent)),
                    onPressed: () {
                      updateNote();
                    },
                    child: Text("Edit Note")),
              ),
              SizedBox(
                width: 20.0,
              ),
              Container(
                width: 120.0,
                height: 40.0,
                child: ElevatedButton(
                    style: ButtonStyle(
                        backgroundColor:
                            MaterialStateProperty.all(Colors.redAccent)),
                    onPressed: () {
                      deleteNote();
                    },
                    child: Text("Delete Note")),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void updateNote() async {
    NoteModel model = new NoteModel(
      id: widget.data.id,
      title: title.text,
      body: body.text,
    );

    await NoteViewModel()
        .updateData(model)
        .then((value) => Navigator.of(context).pop());
  }

  void deleteNote() async {
    await NoteViewModel()
        .deleteData(widget.data)
        .then((value) => Navigator.of(context).pop());
  }
}
