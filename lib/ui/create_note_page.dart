import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:simple_note/model/note_model.dart';
import 'package:simple_note/view_model/note_view_model.dart';

class CreateNotePage extends StatefulWidget {
  @override
  _CreateNotePageState createState() => _CreateNotePageState();
}

class _CreateNotePageState extends State<CreateNotePage> {
  TextEditingController title = new TextEditingController();
  TextEditingController body = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          backgroundColor: Colors.orangeAccent,
          title: Text("Create Note"),
        ),
        body: Container(
          margin: EdgeInsets.all(10.0),
          child: Column(
            children: [
              Container(
                padding: EdgeInsets.all(5.0),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20.0),
                    border: Border.all(color: Colors.orangeAccent, width: 2.0)),
                child: TextField(
                  style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
                  maxLength: 20,
                  controller: title,
                  decoration: InputDecoration(
                      hintText: "title", border: InputBorder.none),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                padding: EdgeInsets.all(5.0),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20.0),
                    border: Border.all(color: Colors.orangeAccent, width: 2.0)),
                child: TextField(
                  style: TextStyle(fontSize: 16.0),
                  minLines: 5,
                  maxLines: null,
                  controller: body,
                  decoration: InputDecoration(
                      hintText: "body", border: InputBorder.none),
                ),
              )
            ],
          ),
        ),
        bottomNavigationBar: Padding(
          padding: EdgeInsets.only(bottom: 30.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                width: 120.0,
                height: 40,
                child: ElevatedButton(
                    style: ButtonStyle(
                        backgroundColor:
                            MaterialStateProperty.all(Colors.orangeAccent)),
                    onPressed: () {
                      saveNote();
                    },
                    child: Text("Create Note")),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void saveNote() async {
    NoteModel model = new NoteModel(
      title: title.text,
      body: body.text,
    );

    await NoteViewModel()
        .insertData(model)
        .then((value) => Navigator.of(context).pop());
  }
}
