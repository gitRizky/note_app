class NoteModel {
  int id;
  String title;
  String body;

  NoteModel({
    this.id,
    this.title,
    this.body,
  });

  Map<String, dynamic> toMap() {
    Map<String, dynamic> data = Map<String, dynamic>();
    data['id'] = id;
    data['title'] = title;
    data['body'] = body;
    return data;
  }

  NoteModel.fromMap(Map<String, dynamic> data) {
    this.id = data['id'];
    this.title = data['title'];
    this.body = data['body'];
  }
}
