import 'package:simple_note/database/database_repository.dart';
import 'package:simple_note/model/note_model.dart';

class NoteViewModel {
  NoteViewModel();

  Future<int> insertData(NoteModel model) async {
    return await DatabaseRepository().inserData(model);
  }

  Future<List<NoteModel>> readData() async {
    return await DatabaseRepository().readData();
  }

  Future<int> updateData(NoteModel model) async {
    return await DatabaseRepository().updateData(model);
  }

  Future<int> deleteData(NoteModel model) async {
    return await DatabaseRepository().deleteData(model);
  }

  Future<List<NoteModel>> searchData(String keyword) async {
    return await DatabaseRepository().searchData(keyword);
  }
}
