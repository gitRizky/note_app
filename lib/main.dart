import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:simple_note/bloc/list_bloc.dart';
import 'package:simple_note/ui/main_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      child: MaterialApp(home: MainPage()),
      providers: [
        BlocProvider(create: (context) => PostBloc()..add(PostEvent()))
      ],
    );
  }
}
