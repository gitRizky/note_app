import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class DataBaseHelper {
  String dataBaseName = "db_note";
  String tableName = "data_note";

  Future<Database> initDatabase() async {
    return openDatabase(join(await getDatabasesPath(), "$dataBaseName"),
        version: 1, onCreate: createTable);
  }

  void createTable(Database db, int version) async {
    String syntaxQuery = '''CREATE TABLE "$tableName" (
	  "id"	INTEGER,
	  "title" TEXT,
	  "body"	TEXT,
	  PRIMARY KEY("id" AUTOINCREMENT));''';
    await db.execute(syntaxQuery);
  }
}
