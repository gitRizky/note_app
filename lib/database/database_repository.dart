import 'package:simple_note/model/note_model.dart';
import 'package:sqflite/sqflite.dart';

import 'database_helper.dart';

class DatabaseRepository {
  DataBaseHelper _databaseHelper = new DataBaseHelper();
  Future<int> inserData(NoteModel model) async {
    final Database db = await _databaseHelper.initDatabase();
    final result = await db.insert(_databaseHelper.tableName, model.toMap(),
        conflictAlgorithm: ConflictAlgorithm.replace);
    return result;
  }

  Future<List<NoteModel>> readData() async {
    final Database db = await _databaseHelper.initDatabase();
    final List<Map<String, dynamic>> datas =
        await db.query(_databaseHelper.tableName);
    return List.generate(datas.length, (index) {
      return NoteModel.fromMap(datas[index]);
    });
  }

  Future<int> updateData(NoteModel model) async {
    final Database db = await _databaseHelper.initDatabase();
    final result = await db.update(_databaseHelper.tableName, model.toMap(),
        where: "id = ?", whereArgs: [model.id]);
    return result;
  }

  Future<int> deleteData(NoteModel model) async {
    final Database db = await _databaseHelper.initDatabase();
    final result = await db.delete(_databaseHelper.tableName,
        where: 'id = ?', whereArgs: [model.id]);
    return result;
  }

  Future<List<NoteModel>> searchData(String keyword) async {
    final Database db = await _databaseHelper.initDatabase();
    String rawQuery =
        'SELECT * FROM ${_databaseHelper.tableName} WHERE nama like "%$keyword%"';
    final List<Map<String, dynamic>> datas = await db.rawQuery(rawQuery);
    final result = List.generate(datas.length, (index) {
      return NoteModel.fromMap(datas[index]);
    });

    return result;
  }
}
